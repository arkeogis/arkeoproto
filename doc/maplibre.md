# Maplibre

Open-source mapping libraries for web and mobile app developers
https://maplibre.org/about/

## Implementation in Arkeo

After many tests, plugins like [react-map-gl](https://visgl.github.io/react-map-gl/docs/get-started/get-started) has to be abandoned. There are two reasons for this :
 - Performances : with the implementation of react-map-gl, the rendering of lot of poi in a map was slower.
 - Code : Most of examples, implementations, are not done using this plugin. And implementing them using this plugin involved to write all the code in a React style, which can be really time consuming.
 
 The implementation in Arkeo is then "from scratch".

 Here is a short example :

 ```jsx
import React, { useRef, useEffect, useState } from 'react';
import maplibregl from 'maplibre-gl'
import 'maplibre-gl/dist/maplibre-gl.css'

function ScratchMap(props) {
  const mapContainer = useRef(null);
  const rmap = useRef(null);
  const [lng, setLng] = useState(10);
  const [lat, setLat] = useState(45);
  const [zoom, setZoom] = useState(3);

  const { loading, error, data } = useQuery(GET_SITES_QUERY, {
    variables: { }
  })

  const geojson_result = {
    "type": "FeatureCollection",
    "features": data && data.site ? data.site.map(site => {
      return {
        "type": "Feature",
        "properties": {
          "name": site.name,
          "mag": site.start_date1,
        },
        "geometry": {
          "type": "Point",
          "coordinates": site.geom.coordinates
        }
      }
    }) : []
  }

  useEffect(() => {
    if (!data || !data.site) return;
    if (rmap.current) return; // initialize map only once
    const map = rmap.current = new maplibregl.Map({
      container: mapContainer.current,
      style: "http:// basemap url style.json",
      center: [lng, lat],
      zoom: zoom
    });
    map.addControl(new maplibregl.NavigationControl());
 
    map.on('move', () => {
      setLng(map.getCenter().lng.toFixed(4));
      setLat(map.getCenter().lat.toFixed(4));
      setZoom(map.getZoom().toFixed(2));
    });
 
    initClusters(map, geojson_result)
  });

  return (
    <div className="map-container-container">
      <div className="sidebar">
        Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
      </div>
      <div ref={mapContainer} className="map-container" />
    </div>
  )
}
 ```
