# GraphQL Apollo

[GraphQL](https://graphql.org/) is a query language for APIs and a runtime for fulfilling those queries with your existing data. GraphQL provides a complete and understandable description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools.

The [Apollo](https://www.apollographql.com/) Supergraph Platform unifies GraphQL across your apps and services, unlocking faster delivery for your engineering teams.

## GraphQL and Apollo in Arkeo Implementation

We have to define queries in `web-app/src/lib/queries`. Here is an example of a querie that list all databases :

```js
import { gql } from '@apollo/client';

export const GET_DATABASES_QUERY = gql`
  query GetDatabases($public: Boolean! = true) {
    database(where: {public: {_eq: $public}, published: {_eq: true}}, order_by: {name: asc}) {
      name
      id
    }
  }
`;
```

Then, you can use the query in a React Component :

```js
import React from 'react'
import PropTypes from "prop-types";
import { useQuery } from '@apollo/client'
import { GET_DATABASES_QUERY } from '../../lib/queries/database'

/**
 * Component that display a list of databases
 */
const DatabasesList = ({isPublic=false}) => {
    const { loading, error, data } = useQuery(GET_DATABASES_QUERY, {
        variables: { public: isPublic }
    })

    if (loading) {
        return (
            <div>Loading...</div>
        )
    }

    if (error) {
        return (
            <div className="error">Error: {error.message}</div>
        )
    }

    return (
        <ul>
            {data.database.map((database, index) => (
                <li key={database.id}>{database.name}</li>
            ))}            
        </ul>
    )
}

DatabasesList.propTypes = {
    /**
     * Filter the listed database with the public field set to this value (default is true)
     */
    isPublic: PropTypes.bool,
}

export default DatabasesList

```
