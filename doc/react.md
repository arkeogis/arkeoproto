# React-JS in Arkeo

## Choices

This project is write using React-JS framework (https://reactjs.org/)

Theres are many way to code using this Framework.

## TypeScript / JS
One of the possible choice is to use typescript, or JS. While theses can be mix together, we choose to use JS, for it's simplicity. Howerver, the use of React PropsTypes for components, and/or states should be the rules.

## Class / Hooks
Hooks were added to React in version 16.8. Hooks allow function components to have access to state and other React features. Because of this, class components are generally no longer needed.

Here is an exemple of a Component Code :

```jsx
import React from 'react'
import PropTypes from 'prop-types'

const Truc = props => {
  return (
    <div>Hello, {props.name}</div>
  )
}

Truc.propTypes = {
    name: PropTypes.string.isRequired
}

export default Truc
```


## Rooting
Create React App doesn't include page routing. React Router is the most popular solution, this is also our choice.

