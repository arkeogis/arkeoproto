# React routing

We use [react-router-dom v6](https://github.com/remix-run/react-router)

## Example adding a page

Import a component (Here, "About") and add the root in App.js :
```jsx
import About from './components/About'

<Routes>
  <Route exact path="/" element={<Main/>}/>
  <Route path="/about" element={<About/>}/>
</Routes>
```
