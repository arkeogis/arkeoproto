import create from "zustand"


const useStore = create((set) => ({
    basemap: "https://basemaps.cartocdn.com/gl/positron-gl-style/style.json",
    setBasemap: basemap => set({basemap}),
}))


export default useStore;
