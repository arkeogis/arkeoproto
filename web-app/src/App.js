// react
import { Suspense } from 'react'
import {
  Routes,
  Route,
} from "react-router-dom"
//

// css
import './App.css'
//

// components
import Header from './components/Header/Header'
import Main from './components/Main/Main'
import About from './components/About/About'
import Boot from './components/Boot/Boot'
import Loader from './components/Loader/Loader'
//

function App() {
  return (
    <Suspense fallback={<Loader />}>
      <div className="App">
        <Header></Header>
        <Routes>
          <Route exact path="/" element={<Main/>}/>
          <Route path="/about" element={<About/>}/>
          <Route path="/boot" element={<Boot/>}/>
        </Routes>
      </div>
    </Suspense>
  )
}

export default App;
