import { gql } from '@apollo/client';

export const GET_DATABASES_QUERY = gql`
  query GetDatabases($public: Boolean! = true) {
    database(where: {public: {_eq: $public}, published: {_eq: true}}, order_by: {name: asc}) {
      name
      id
    }
  }
`;
