import { gql } from '@apollo/client';

export const GET_SITES_QUERY = gql`
  query GetSites {
    site(limit: 10000) {
      geom
      id
      name
      start_date1
      start_date2
    }
  }
`;
