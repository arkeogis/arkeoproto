import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types'
import maplibregl from 'maplibre-gl'
import 'maplibre-gl/dist/maplibre-gl.css'

// graphql
import { useQuery } from '@apollo/client'
import { GET_SITES_QUERY } from '../../lib/queries/sites'

import './ScratchMap.css'
import Loader from '../Loader/Loader';
import useStore from '../../store'

function ScratchMap(props) {
  const basemap = useStore(state => state.basemap)

  const mapContainer = useRef(null);
  const rmap = useRef(null);
  const [lng, setLng] = useState(10);
  const [lat, setLat] = useState(45);
  const [zoom, setZoom] = useState(3);

  const { loading, error, data } = useQuery(GET_SITES_QUERY, {
    variables: { }
  })

  const geojson_result = {
    "type": "FeatureCollection",
    "features": data && data.site ? data.site.map(site => {
      return {
        "type": "Feature",
        "properties": {
          "name": site.name,
          "mag": site.start_date1,
        },
        "geometry": {
          "type": "Point",
          "coordinates": site.geom.coordinates
        }
      }
    }) : []
  }

  useEffect(() => {
    if (!rmap.current) return; // wait for map to initialize

    // because map.setStyle() remove all layers, it's simpler and faster to remove the map object and build a new one.
    // https://github.com/mapbox/mapbox-gl-js/issues/4006
    mapContainer.current.html='';
    rmap.current = null; // force re-init a new map
  }, [basemap])

  useEffect(() => {
    if (!data || !data.site) return;
    if (rmap.current) return; // initialize map only once
    const map = rmap.current = new maplibregl.Map({
      container: mapContainer.current,
      style: basemap,
      center: [lng, lat],
      zoom: zoom
    });
    map.addControl(new maplibregl.NavigationControl());

    initClusters(map, geojson_result)
  });

  useEffect(() => {
    if (!rmap.current) return; // wait for map to initialize
    const map=rmap.current;
    map.on('move', () => {
      setLng(map.getCenter().lng.toFixed(4));
      setLat(map.getCenter().lat.toFixed(4));
      setZoom(map.getZoom().toFixed(2));
    });
  }, [ rmap ]);

  if (error) {
    return (
      <div className="error">
        {error.message}
      </div>
    )
  }

  if (loading) {
    return (
      <div className="map-container-loading">
        <Loader />
      </div>
    )
  }

  return (
    <div className="map-container-container">
      <div className="sidebar">
        Longitude: {lng} | Latitude: {lat} | Zoom: {zoom}
      </div>
      <div ref={mapContainer} className="map-container" />
    </div>
  )
}

function initClusters(map, geojson_result) {
  // filters for classifying earthquakes into five categories based on magnitude
  const mag1 = ['<', ['get', 'mag'], -2000];
  const mag2 = ['all', ['>=', ['get', 'mag'], -2000], ['<', ['get', 'mag'], -1500]];
  const mag3 = ['all', ['>=', ['get', 'mag'], -1500], ['<', ['get', 'mag'], -1000]];
  const mag4 = ['all', ['>=', ['get', 'mag'], -1000], ['<', ['get', 'mag'], -500]];
  const mag5 = ['all', ['>=', ['get', 'mag'], -500], ['<', ['get', 'mag'], 0]];
  const mag6 = ['all', ['>=', ['get', 'mag'], 0], ['<', ['get', 'mag'], 500]];
  const mag7 = ['all', ['>=', ['get', 'mag'], 500], ['<', ['get', 'mag'], 1000]];
  const mag8 = ['all', ['>=', ['get', 'mag'], 1000], ['<', ['get', 'mag'], 1500]];
  const mag9 = ['all', ['>=', ['get', 'mag'], 1500], ['<', ['get', 'mag'], 1700]];
  const mag0 = ['>=', ['get', 'mag'], 1700];
  
  // colors to use for the categories
  const colors = ['#fed976', '#feb24c', '#fd8d3c', '#fc4e2a', '#e31a1c', '#ff0000', '#00ff00', '#0000ff', '#00ffff', '#ff0000', '#ff00ff'];
  
  map.on('load', function () {
    // add a clustered GeoJSON source for a sample set of earthquakes
    map.addSource('earthquakes', {
      'type': 'geojson',
      'data':
      geojson_result,
      'cluster': true,
      'clusterRadius': 60,
      'clusterMaxZoom': 10,
      'clusterProperties': {
        // keep separate counts for each magnitude category in a cluster
        'mag1': ['+', ['case', mag1, 1, 0]],
        'mag2': ['+', ['case', mag2, 1, 0]],
        'mag3': ['+', ['case', mag3, 1, 0]],
        'mag4': ['+', ['case', mag4, 1, 0]],
        'mag5': ['+', ['case', mag5, 1, 0]],
        'mag6': ['+', ['case', mag6, 1, 0]],
        'mag7': ['+', ['case', mag7, 1, 0]],
        'mag8': ['+', ['case', mag8, 1, 0]],
        'mag9': ['+', ['case', mag9, 1, 0]],
        'mag0': ['+', ['case', mag0, 1, 0]],
      }
    });

    // circle and symbol layers for rendering individual earthquakes (unclustered points)
    map.addLayer({
      'id': 'earthquake_circle',
      'type': 'circle',
      'source': 'earthquakes',
      'filter': ['!=', 'cluster', true],
      //'filter': ['ALL', ['cluster', true], ['<', ['zoom'], 4]],
      'paint': {
        'circle-color': [
          'case',
          mag1,
          colors[0],
          mag2,
          colors[1],
          mag3,
          colors[2],
          mag4,
          colors[3],
          mag5,
          colors[4],
          mag6,
          colors[5],
          mag7,
          colors[6],
          mag8,
          colors[7],
          mag9,
          colors[8],
          mag0,
          colors[9],
          colors[10]
        ],
        'circle-opacity': 0.6,
        'circle-radius': 12
      }
    });
    map.addLayer({
      'id': 'earthquake_label',
      'type': 'symbol',
      'source': 'earthquakes',
      'filter': ['!=', 'cluster', true],
      'layout': {
        'text-field': [
          'number-format',
          ['get', 'mag'],
          { 'min-fraction-digits': 1, 'max-fraction-digits': 1 }
        ],
        'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
        'text-size': 10
      },
      'paint': {
        'text-color': [
          'case',
          ['<', ['get', 'mag'], 0],
          'black',
          'white'
        ]
      }
    });
    
    // objects for caching and keeping track of HTML marker objects (for performance)
    const markers = {};
    let markersOnScreen = {};
    
    function updateMarkers() {
      const newMarkers = {};
      const features = map.querySourceFeatures('earthquakes');
      
      // for every cluster on the screen, create an HTML marker for it (if we didn't yet),
      // and add it to the map if it's not there already
      for (const feature of features) {
        const coords = feature.geometry.coordinates;
        const props = feature.properties;
        if (!props.cluster) continue;
        const id = props.cluster_id;

        let marker = markers[id];
        if (!marker) {
          const el = createDonutChart(props);
          marker = markers[id] = new maplibregl.Marker({
            element: el
          }).setLngLat(coords);
        }
        newMarkers[id] = marker;
        
        if (!markersOnScreen[id]) marker.addTo(map);
      }

      // for every marker we've added previously, remove those that are no longer visible
      for (const id in markersOnScreen) {
        if (!newMarkers[id]) markersOnScreen[id].remove();
      }
      markersOnScreen = newMarkers;
    }
    
    // after the GeoJSON data is loaded, update markers on the screen and do so on every map move/moveend
    map.on('data', function (e) {
      if (e.sourceId !== 'earthquakes' || !e.isSourceLoaded) return;
      
      map.on('move', updateMarkers);
      map.on('moveend', updateMarkers);
      updateMarkers();
    });
  });
  
  // code for creating an SVG donut chart from feature properties
  function createDonutChart(props) {
    const offsets = [];
    const counts = [
      props.mag1,
      props.mag2,
      props.mag3,
      props.mag4,
      props.mag5,
      props.mag6,
      props.mag7,
      props.mag8,
      props.mag9,
      props.mag0
    ];
    let total = 0;
    for (const count of counts) {
      offsets.push(total);
      total += count;
    }
    let fontSize =
      total >= 1000 ? 22 : total >= 100 ? 20 : total >= 10 ? 18 : 16;
    let r = total >= 1000 ? 50 : total >= 100 ? 32 : total >= 10 ? 24 : 18;
    let r0 = Math.round(r * 0.6);
    let w = r * 2;
    
    let html =
    '<div><svg width="' +
    w +
    '" height="' +
    w +
    '" viewbox="0 0 ' +
    w +
    ' ' +
    w +
    '" text-anchor="middle" style="font: ' +
    fontSize +
    'px sans-serif; display: block">';
    
    for (let i = 0; i < counts.length; i++) {
      html += donutSegment(
        offsets[i] / total,
        (offsets[i] + counts[i]) / total,
        r,
        r0,
        colors[i]
        );
      }
      html +=
      '<circle cx="' +
      r +
      '" cy="' +
      r +
      '" r="' +
      r0 +
      '" fill="white" /><text dominant-baseline="central" transform="translate(' +
      r +
      ', ' +
      r +
      ')">' +
      total.toLocaleString() +
      '</text></svg></div>';
      
      let el = document.createElement('div');
      el.innerHTML = html;
      return el.firstChild;
    }
    
    function donutSegment(start, end, r, r0, color) {
      if (end - start === 1) end -= 0.00001;
      const a0 = 2 * Math.PI * (start - 0.25);
      const a1 = 2 * Math.PI * (end - 0.25);
      const x0 = Math.cos(a0),
            y0 = Math.sin(a0);
      const x1 = Math.cos(a1),
            y1 = Math.sin(a1);
      const largeArc = end - start > 0.5 ? 1 : 0;
      
      return [
        '<path d="M',
        r + r0 * x0,
        r + r0 * y0,
        'L',
        r + r * x0,
        r + r * y0,
        'A',
        r,
        r,
        0,
        largeArc,
        1,
        r + r * x1,
        r + r * y1,
        'L',
        r + r0 * x1,
        r + r0 * y1,
        'A',
        r0,
        r0,
        0,
        largeArc,
        0,
        r + r0 * x0,
        r + r0 * y0,
        '" fill="' + color + '" />'
      ].join(' ');
  }
}


ScratchMap.propTypes = {}

export default ScratchMap
