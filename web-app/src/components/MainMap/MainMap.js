import * as React from 'react'
import PropTypes from 'prop-types'
import Map, {Layer, Marker, Source} from 'react-map-gl'
import maplibregl from 'maplibre-gl'
import 'maplibre-gl/dist/maplibre-gl.css'
import useStore from '../../store'

// graphql
import { useQuery } from '@apollo/client'
import { GET_SITES_QUERY } from '../../lib/queries/sites'


const MainMap = (props) => {
  const basemap = useStore(state => state.basemap)
  const { loading, error, data } = useQuery(GET_SITES_QUERY, {
    variables: { }
  })

  console.log("p1", data && data.site ? data.site.length : "no")
  const geojson_result = {
    "type": "FeatureCollection",
    "features": data && data.site ? data.site.map(site => {
      return {
        "type": "Feature",
        "properties": {
          "name": site.name
        },
        "geometry": {
          "type": "Point",
          "coordinates": site.geom.coordinates
        }
      }
    }) : []
  }
  console.log("p2", data && data.site ? data.site.length : "no")
  //console.log(data.site)

  const layerStyle = {
    id: 'point',
    type: 'circle',
    paint: {
      'circle-radius': 10,
      'circle-color': '#007cbf'
    }
  };

  return     <Map
  initialViewState={{
    latitude: 48.5691668,
    longitude: 7.6920398,
    zoom: 8
  }}
  mapLib={maplibregl}
  style={{flex: "1", width: "auto", height: "auto"}}
  mapStyle={basemap}
>
  <Source id="arkeosearchresult" type="geojson" data={geojson_result} cluster={true} clusterRadius={80}>
    <Layer {...layerStyle}/>
  </Source>
  <Marker longitude={7.6920398} latitude={48.5691668} color="red" />
</Map>
}

MainMap.propTypes = {
}

MainMap.defaultProps = {
}

export default MainMap