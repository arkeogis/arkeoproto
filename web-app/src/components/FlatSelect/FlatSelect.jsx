// react
import React, { useState } from 'react';
import PropTypes from 'prop-types';
//

// bootstrap
import Collapse from 'react-bootstrap/Collapse';
//

// icons
import { FaChevronDown  } from 'react-icons/fa';
//

import './FlatSelect.css';

function FlatSelect(props) {
  const [ opened, setOpened ] = useState(false);
  const [ selected, setSelected ] = useState(false);
  const { select, label, children, leaf=false } = props;
  return (
    <div className="FlatSelect">
      <div className={`FlatSelectSelect ${leaf ? 'leaf': 'notleaf'}`}
        {...select}
      >
        <button className={`label btn ${selected ? 'btn-primary' : 'btn-secondary'}`} onClick={() => setSelected(!selected)}>
          {label}
        </button>
        { !leaf ? (
          <button
          className={`icon btn ${selected ? 'btn-primary' : 'btn-secondary'}`}
          onClick={() => setOpened(!opened)}
        >
          <FaChevronDown
            className={`${opened ? 'opened' : 'closed'}`}
          />
        </button>
        ) : ''}
      </div>
      <Collapse in={opened} className="FlatSelectContent">
        <div>
          {children ? children : ""}
        </div>
      </Collapse>
    </div>
  );
}

FlatSelect.propTypes = {
  select: PropTypes.object,
  label: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  leaf: PropTypes.bool,
};

export default FlatSelect;
