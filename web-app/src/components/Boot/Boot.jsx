import React, { useRef, useState } from 'react'
import { Button, Stack, Badge, Offcanvas } from 'react-bootstrap';

import FlatSelect from '../FlatSelect/FlatSelect';

export const ScrollTest = () => {
  return (
    <div style={{height: 200, width: 200, background: "#ffffff", overflowY: "scroll"}}>
      <div style={{height: 300, width: 150, background: "#3f3f3f"}}></div>
    </div>
  )
}

export const LeftSideBar = (props) => {
  const [show, setShow] = useState(false);
    return (
    <div>
      <Offcanvas show={show} scroll={true} backdrop={false} container={props.container}>
        <p>Coucou</p>
      </Offcanvas>
      <button onClick={() => setShow(!show)}>Toggle</button>
    </div>
  )
}

export const Boot = () => {
  const refSideBar = useRef();
  return (
    <div>
      <div ref={refSideBar} className="MonSideBarContainer"></div>
      <div>
        <Stack direction="horizontal" gap={2}>
          <Button as="a" variant="primary">
            Button as link
          </Button>
          <Button as="a" variant="success">
            Button as link
          </Button>
          <Button>A button</Button>
        </Stack>
      </div>

      <div>
        <Badge bg="primary">Primary</Badge>{' '}
        <Badge bg="secondary">Secondary</Badge>{' '}
        <Badge bg="success">Success</Badge> <Badge bg="danger">Danger</Badge>{' '}
        <Badge bg="warning" text="dark">
          Warning
        </Badge>{' '}
        <Badge bg="info">Info</Badge>{' '}
        <Badge bg="light" text="dark">
          Light
        </Badge>{' '}
        <Badge bg="dark">Dark</Badge>
      </div>

      <div>
        <Button variant="primary">Primary</Button>{' '}
        <Button variant="secondary">Secondary</Button>{' '}
        <Button variant="success">Success</Button>{' '}
        <Button variant="warning">Warning</Button>{' '}
        <Button variant="danger">Danger</Button>{' '}
        <Button variant="info">Info</Button>{' '}
        <Button variant="light">Light</Button>{' '}
        <Button variant="dark">Dark</Button> <Button variant="link">Link</Button>
      </div>

      <ScrollTest/>

      <FlatSelect label="Open Me" leaf={false}>
        <FlatSelect label="Option 1" leaf={true}/>
        <FlatSelect label="Option 2" leaf={false}>
          <FlatSelect label="SubOption 1" leaf={true}/>
          <FlatSelect label="SubOption 2" leaf={true}/>
        </FlatSelect>
        <FlatSelect label="Option 3" leaf={true}/>
      </FlatSelect>

      <LeftSideBar container={refSideBar}/>
    </div>
  )
}


export default Boot
