import React from 'react'

export const About = () => {
    return (
        <div>
            <p>ArkeoProto is a prototype for future arkeogis developpements</p>
            <p>ArkeoProto is based on react.js for frontend (client), hasura and postgresql database for backend</p>
        </div>
    )
}

export default About
