import React from 'react'
import './Main.css'
import DatabasesList from '../DatabasesList/DatabasesList'
import MainMap from '../MainMap/ScratchMap'
//import MainMap from '../MainMap'

/**
 * This is the Main component, that will be visible when landing on this site
 */

const Main = () => {
    return (
        <div className="Main">
            <MainMap/>
        </div>
    )
}
//<DatabasesList isPublic={true}/>

export default Main
