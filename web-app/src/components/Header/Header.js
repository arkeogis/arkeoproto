import React from 'react'
import './Header.css'
import BasemapSelect from '../BasemapSelect/BasemapSelect'
import LangSelect from '../LangSelect/LangSelect'

const Header = () => {
    return (
        <div className="ComponentHeader">
            <header>
                <div className="header-title">ArkeoProto</div>
                <div className="header-langselect"><LangSelect/></div>
                <div className="header-basemap"><BasemapSelect/></div>
            </header>
        </div>
    )
}

export default Header