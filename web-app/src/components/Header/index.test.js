import { render, screen } from '@testing-library/react';
import Header from '.';

test('renders header text element', () => {
  render(<Header />);
  const textElement = screen.getByText(/ArkeoProto/i);
  expect(textElement).toBeInTheDocument();
});
