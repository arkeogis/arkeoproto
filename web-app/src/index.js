import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';

import create from 'zustand'

import { BrowserRouter } from "react-router-dom";

import { ApolloClient, ApolloProvider, InMemoryCache, HttpLink } from "@apollo/client";

import './i18n';

// Apollo Client (GRAPHQL) SETUP
// This setup is only needed once per application;
const apolloClient = new ApolloClient({
    cache: new InMemoryCache(),
    link: new HttpLink({
        uri: "http://localhost:8080/v1/graphql",
        //uri: "http://home.keblo.net:5001/v1/graphql",
      }),
});

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ApolloProvider client={apolloClient}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ApolloProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
